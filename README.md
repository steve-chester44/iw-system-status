# IW System Status

aka "doc"

*A node/express API for serving the status of configured services*


## Getting Started
* `yarn install` (duh)


* The main process is started via app.js which will spin up a node/express server on the port in settings.js
& It's designed with the use of environment variables in mind but current is hard coded for a single instance in mind (so change the port in settings.internal)
* To configure services, edit settings.js


### Available endpoints:
* GET /status - returns an object

```
{
  "lastUpdate": "timestamp",
  "services": [
    {
      "name": "channelshd",
      "label": "ChannelsHD",
      "status": "up",
      "statusCode": 200
    }
  ]
}
```


### TODO
* Finish plugin support