/* jshint esversion:6 */
const mergeDeep = require("./lib/utils").mergeDeep;

class Settings {
    constructor() {
        this.s = {};
        this.init();
    }

    init() {
        let env = {};

        let defaults = {
            title: "IW Services Status",
            hostname: "127.0.0.1",
            port: "3000",
            services: [],
            serviceInterval: 40000
        };

        env.internal = {
            port: "3000",
            hostname: "0.0.0.0",
            services: [
                {
                    name: "google-dns",
                    label: "Google DNS",
                    check: "ping",
                    host: "127.0.0.1"
                },
                {
                    name: "bitbucket",
                    label: "Bitbucket",
                    check: "http",
                    host: "bitbucket.iwstorage.com",
                    port: "7990",
                    path: "/",
                    group: "internal"
                },
                {
                    name: "iw",
                    label: "IW homepage",
                    check: "http",
                    host: "50.57.251.10",
                    port: "80",
                    path: "/",
                    group: "webpages"
                },
                {
                    name: "channelshd",
                    label: "ChannelsHD",
                    check: "http",
                    host: "iw.channelshd.com",
                    port: "80",
                    path: "/",
                    group: "webpages"
                },
                {
                    name: "webcache",
                    label: "WebcacheHD",
                    check: "ping",
                    host: "209.166.158.86"
                },
                {
                    name: "eventsapp",
                    label: "Events App",
                    host: "10.16.4.195",
                    check: "ping",
                    group: "sandboxes"
                },
                {
                    name: "menusapp",
                    label: "Menus App",
                    host: "10.16.4.200",
                    check: "ping",
                    group: "sandboxes"
                },
                {
                    name: "alertsapp",
                    label: "Alerts App",
                    host: "10.16.4.201",
                    check: "ping",
                    group: "sandboxes"
                },
                {
                    name: "sharefile",
                    label: "Sharefile",
                    host: "10.16.4.204",
                    check: "ping",
                    group: "sandboxes"
                }
            ]
        };

        // if (process.env.APP_ENC) {
        // this.s = mergeDeep(defaults, settings[process.env.APP_ENV]);
        // }
        this.s = mergeDeep(defaults, env.internal);
    }

    set(key, value) {
        this.s[key] = value;
    }

    get() {
        return this.s;
    }
}

module.exports = Settings;
