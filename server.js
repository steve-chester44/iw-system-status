/* jshint esversion:6 */
const express = require("express");
const allowCrossDomain = require("./lib/middleware/allowCrossDomain").allowCrossDomain;
const app = express();

const Api = require("./api");
const api = new Api();

class Server {
	constructor(settings) {
		this.configure(settings);
	}

	configure(settings) {
		this.settings = settings;
	}

	start() {
		app.use(allowCrossDomain);
		app.listen(this.settings.port, this.settings.hostname);
		console.log(`Server running at http://${this.settings.hostname}:${this.settings.port}`);

		app.get("/healthy", (req, res) => {
			res.send("ok", 200);
		});

		api.setup(this.settings);
		api.startChecks();

		app.get("/status", (req, res) => api.status(req, res));

		// TODO --  Plugin Support
		// api.on('routeFromPlugin', (route) => {
		// 	app.get(route.path, route.binding);
		// });
	}
}

module.exports = Server;
