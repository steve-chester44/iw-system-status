/* jshint esversion:6 */
const fs = require("fs");
const path = require("path");

class Api {
	constructor() {
		this.props = {
			cache: path.join(__dirname, 'cache.json'),
			settings: undefined,
			sources: {},
			status: {
				lastUpdate: Math.floor(new Date() / 1000),
				services: []
			}
		};
	}

	configure(settings) {
		this.props.settings = settings;
	}

	setup(settings) {
		this.configure(settings);

		let sourceFiles = fs.readdirSync(path.join(__dirname, "lib", "sources"));

		for (let i = 0; i < sourceFiles.length; i++) {
			let source = sourceFiles[i];
			source = source.substr(0, source.length - 3);

			this.props.sources[source] = require(path.join(
				__dirname,
				"lib",
				"sources",
				source
			));
		}
	}

	async checkServices() {
		this.props.status.lastUpdate = Math.floor(new Date() / 1000);

		for (let i = 0; i < this.props.settings.services.length; i++) {
			let service = this.props.settings.services[i];

			this.props.status.services[i] = {
				name: service.name,
				label: service.label,
				group: service.group
			};
						
			let Source = this.props.sources[this.props.settings.services[i].check];
			let source = new Source();
			let status;

			try {
				let res = await source.check(service);
				this.props.status.services[i].status = res.status;
				this.props.status.services[i].statusCode = res.statusCode;
			} catch(e) {
				this.props.status.services[i].status = e.status;
				this.props.status.services[i].statusCode = e.statusCode;
				this.props.status.services[i].message = e.message;
			}
		}

		fs.writeFileSync(this.props.cache, JSON.stringify(this.props.status));
	}

	startChecks() {
		this.serviceCheckInterval = setInterval(this.checkServices.bind(this), this.props.settings.serviceInterval);
		this.checkServices();
	}
	
	stopChecking() {
		clearInterval(this.checkServices);
	}

	status(req, res) {
		if (!fs.existsSync(this.props.cache)) {
			res.status(404);
			return;
		}

		let cache = fs.readFileSync(this.props.cache);
		res.status(200);
		res.json(JSON.parse(cache));
	}
}

module.exports = Api;