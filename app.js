/* jshint esversion:6 */
const Server = require('./server');
const Settings = require('./settings');
const settings = new Settings().get();

settings.port = process.env.PORT || settings.port;

const server = new Server(settings);
server.start();