/* jshint esversion:6 */
exports.isObject = (item) => (item && typeof item === 'object' && !Array.isArray(item));

exports.mergeDeep = (target, ...sources) => {
    if (!sources.length) return target;
    const source = sources.shift();

    if (exports.isObject(target) && exports.isObject(source)) {
        for (const key in source) {
            if (exports.isObject(source[key])) {
                if (!target[key]) Object.assign(target, {
                    [key]: {}
                });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, {
                    [key]: source[key]
                });
            }
        }
    }
    return exports.mergeDeep(target, ...sources);
};


exports.generateRequestUrl = (opts) => {
    if (!opts.check) {
        return false;
    }

    return `${opts.check}://${opts.host}:${opts.port}${opts.path}`;
};