/* jshint esversion:6 */
const mergeDeep = require("../utils").mergeDeep;
const tcpp = require("tcp-ping");

class PingSource {
	constructor() {}

	check(service) {
		let httpOptions = {
			host: service.host,
			port: service.port,
			path: service.path,
			headers: service.headers,
			check: service.check
		};

		let options = mergeDeep(httpOptions, service.options);

		return new Promise((resolve, reject) => {
			tcpp.ping({ address: options.host }, function(err, data) {
				service.status = data.max ? "up" : "down";

				if (err || !data.max) {
                    reject({ status: "down", statusCode: 0, message: err });
                    return;
				}

				resolve({ status: data.max ? "up" : "down", statusCode: 0 });
			});
		});
	}
}

module.exports = PingSource;
