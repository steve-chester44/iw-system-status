/* jshint esversion:6 */

const request = require("request");
const mergeDeep = require("../utils").mergeDeep;
const httpStatusCode = require("../checks/httpStatusCode").execute;
const generateRequestUrl = require("../utils").generateRequestUrl;

class HTTPSource {
    constructor() {}

    check(service) {
        let httpOptions = {
            host: service.host,
            port: service.port,
            path: service.path,
            headers: service.headers,
            check: service.check
        };

        let options = mergeDeep(httpOptions, service.options);

        return new Promise((resolve, reject) => {
            request(generateRequestUrl(options), (err, res, body) => {
                let e = JSON.parse(JSON.stringify(err));

                if (err) {
                    reject({ status: "down", statusCode: 0, message: e.code});
                    return;
                }

                let check = service.callback || httpStatusCode;

                if (service) {
                    check(res, service, (service) => {
                        resolve({ status: service.status, statusCode: service.statusCode });
                    });
                }
            });
        });
    }
}

module.exports = HTTPSource;
