/* jshint esversion:6 */
exports.execute = (res, service, cb) => {

	service = service || {};

	let codes = [
		{
			status: "up",
			codes: [200]
		},
		{
			status: "unknown",
			codes: [301, 302]
		},
		{
			status: "down",
			codes: [503, 404, 500]
		},
		{
			status: "critical",
			codes: []
		}
	];

	let code = codes.find(obj => obj.codes.includes(res.statusCode));

	if (!code) {
		code = codes.find(obj => obj.status === "unknown");
	}

	service.status = code.status;
	service.statusCode = res.statusCode;

	if (cb) { cb(service); }
};